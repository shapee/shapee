/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class TriangleFrame extends JFrame{
JLabel lblBase;
    JLabel lblHight;
    JTextField txtBase;
    JTextField txtHight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(600, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblBase = new JLabel("Base : ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);

        lblHight = new JLabel("Lenght : ", JLabel.TRAILING);
        lblHight.setSize(50, 20);
        lblHight.setLocation(5, 30);
        lblHight.setBackground(Color.WHITE);
        lblHight.setOpaque(true);
        this.add(lblHight);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);

        txtHight = new JTextField();
        txtHight.setSize(50, 20);
        txtHight.setLocation(60, 30);
        this.add(txtHight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle wide = ???"+  "Triangle Lenght = ??? "
                + "area = ???" + "perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(600, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {//Anonymus class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHight = txtHight.getText();
                    double Base = Double.parseDouble(strBase);
                    double Hight = Double.parseDouble(strHight);
                    Triangle tri = new Triangle(Base,Hight);
                    lblResult.setText("Triangle Base = " + String.format("%.2f", tri.getBase())
                            +"Triangle Hight = " + String.format("%.2f", tri.getHight())
                            + "area = " + String.format("%.2f", tri.calArea())
                            + "perimeter = " + String.format("%.2f", tri.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHight.setText("");
                    txtHight.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }

}