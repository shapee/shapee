/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class RectangleFrame extends JFrame {

    JLabel lblWide;
    JLabel lblLenght;
    JTextField txtWide;
    JTextField txtLenght;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(600, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWide = new JLabel("Wide : ", JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        this.add(lblWide);

        lblLenght = new JLabel("Lenght : ", JLabel.TRAILING);
        lblLenght.setSize(50, 20);
        lblLenght.setLocation(5, 30);
        lblLenght.setBackground(Color.WHITE);
        lblLenght.setOpaque(true);
        this.add(lblLenght);

        txtWide = new JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(60, 5);
        this.add(txtWide);

        txtLenght = new JTextField();
        txtLenght.setSize(50, 20);
        txtLenght.setLocation(60, 30);
        this.add(txtLenght);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle wide = ???"+  "Rectangle Lenght = ??? "
                + "area = ???" + "perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(600, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {//Anonymus class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strwide = txtWide.getText();
                    String strLenght = txtLenght.getText();
                    double Wide = Double.parseDouble(strwide);
                    double Lengthy = Double.parseDouble(strLenght);
                    Rectangle rec = new Rectangle(Wide,Lengthy);
                    lblResult.setText("Rectangle wide = " + String.format("%.2f", rec.getWide())
                            +"Rectangle Lenght = " + String.format("%.2f", rec.getLenght())
                            + "area = " + String.format("%.2f", rec.calArea())
                            + "perimeter = " + String.format("%.2f", rec.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error : Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtWide.requestFocus();
                    txtLenght.setText("");
                    txtLenght.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }

}
