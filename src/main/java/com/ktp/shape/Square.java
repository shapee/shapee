/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shape;

/**
 *
 * @author acer
 */
public class Square extends Shape{
    
    private double Side;

    public Square(double side) {
        super("Square");
        this.Side = Side;
    }

    public double getSide() {
        return Side;
    }
     public Square() {
        super("Square");
    }

    @Override
    public double calArea() {
        return Side*Side;
    }

    @Override
    public double calPerimeter() {
        return 4*Side;
    }
    
}
