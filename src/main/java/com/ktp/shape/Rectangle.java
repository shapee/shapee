/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shape;

/**
 *
 * @author acer
 */
public class Rectangle extends Shape{
    
    private double Wide;
    private double Lenght;

    public Rectangle(double Wide,double Lenght) {
        super("Rectangle");
        this.Lenght = Lenght;
        this.Wide = Wide;
    }

    public double getWide() {
        return Wide;
    }

    public double getLenght() {
        return Lenght;
    }
    
    public Rectangle() {
        super("Rectangle");
    }

    @Override
    public double calArea() {
        return Wide*Lenght;
    }

    @Override
    public double calPerimeter() {
        return 2*(Wide+Lenght);
    }
    
}
