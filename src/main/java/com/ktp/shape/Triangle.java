/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shape;

/**
 *
 * @author acer
 */
public class Triangle extends Shape{
    
    private double Base;
    private double Hight;

    public Triangle(double Base,double Hight) {
        super("Triangle");
        this.Base = Base;
        this.Hight = Hight;
    }

    public double getBase() {
        return Base;
    }

    public double getHight() {
        return Hight;
    }
    public Triangle() {
        super("Triangle");
    }

    @Override
    public double calArea() {
        return 0.5*Base*Hight;
    }

    @Override
    public double calPerimeter() {
        return Base*3;
    }
    
}
